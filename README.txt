This module adds plupload field before each field collection, if in this collection there is the image field.
Submit form will add to node new field collection entities for each photos in plupload field.

It is an analog of module field_collection_bulkupload, but for Drupal 8.

Author
======================
- Istislav Verterov <cyb DOT erunit at gmail DOT com>
